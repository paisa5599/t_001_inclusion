sap.ui.define([], function() {
	"use strict";
	return {
		/* 
			Used in table View1 for change the status from X indicator.
		*/
		stateStock: function(iStock) {
			var iVal = parseInt(iStock);

			if (iVal <= 2500) {
				return "Error";
			}

			return "Success";
		},
		/* 
			Used in table View1 when is filtering from View3.
		*/
		factoryDate: function(oDate) {
			var year = oDate.getFullYear(),
				month = (oDate.getMonth() + 1) <= 9 ? "0" + (oDate.getMonth() + 1) : oDate.getMonth() + 1,
				day = (oDate.getDate()) <= 9 ? "0" + oDate.getDate() : oDate.getDate();

			return year + '-' + month + '-' + day;
		},
		/* 
			Pretty format Date.
		*/
		manufDate: function(oDate) {
			var aMonths = ['Ene.', 'Feb.', 'Mar.', 'Abr.', 'May.', 'Jun.', 'Jul.', 'Ago.', 'Sept.', 'Oct.', 'Nov.', 'Dic.'];
			var oDate = new Date(oDate);
			return oDate.getDate() + ' ' + (aMonths[oDate.getMonth()]) + ' ' + oDate.getFullYear();
		},
		/* 
			Object ID Generator
		*/
		generateOID: function() {
			var timestamp = (new Date().getTime() / 1000 | 0).toString(16);
			return timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, function() {
				return (Math.random() * 16 | 0).toString(16);
			}).toLowerCase();
		},
		/*
			Specification Formatter: Specific Requirements
		*/
		price: function(sPrice) {
			if (!sPrice) {
				return "N/A";
			}
			var aPrice = sPrice.split("-");
			var oCurrency = new sap.ui.model.type.Currency({
				showMeasure: false,

			});

			var ret = oCurrency.formatValue([aPrice[0], aPrice[1]], "string");
			var posfix = "$";

			if (aPrice[1].endsWith('USD')) {
				posfix = "u$d";
			}

			return posfix + " " + ret;
		}
	};
});