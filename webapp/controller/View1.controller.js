sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"com/inclusion/T_001_INCLUSION_DEV/model/formatter",
	"sap/ui/export/Spreadsheet",
	"sap/m/MessageToast"
], function(Controller, formatter, Spreadsheet, MessageToast) {
	"use strict";

	return Controller.extend("com.inclusion.T_001_INCLUSION_DEV.controller.View1", {
		formatter: formatter,
		oModel: null,
		oRouter: null,
		onInit: function() {
			this.bindRouteMatched();

			// Load JSON in model
			this.oModel = new sap.ui.model.json.JSONModel();
			this.oModel.loadData("model/data.json");
			this.oModel.setSizeLimit(999);
			this.getView().setModel(this.oModel);
		},
		bindRouteMatched: function() {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getRoute("View1").attachMatched(this._onRouteMatched, this);
			this.oRouter.getRoute("View1Filter").attachMatched(this._onRouteMatchedFilter, this);
		},
		_onRouteMatchedFilter: function(oEvent) {
			// Case filter
			var sObjectId = oEvent.getParameter("arguments").filter;
			if (sObjectId && Object.keys(this.oModel.getData()).length > 0) {
				this.filterTable(sObjectId);
			}
		},
		_onRouteMatched: function(oEvent) {
			// Case create
			var sObjectId = oEvent.getParameter("arguments").row;
			if (sObjectId && Object.keys(this.oModel.getData()).length > 0) {
				this.createNewItem(sObjectId);
			}
		},
		createNewItem: function(sObjectId) {
			var a = JSON.parse(sObjectId);
			this.oModel.setProperty("/", this.oModel.getProperty("/").concat(a));
			this.oModel.setSizeLimit(999);
			this.getView().setModel(this.oModel);
			this.getView().getModel().refresh();
		},
		navNext: function() {
			this.oRouter.navTo("View2");
		},
		onDeleteTable: function(evt) {
			var oContext = evt.getParameter("listItem").getBindingContext();
			var sPath = oContext.getPath();
			var iPos = sPath.split("/")[sPath.split("/").length - 1];

			var oModel = this.getView().getModel().getProperty("/");
			oModel.splice(iPos, 1);

			this.getView().getModel().setProperty("/", oModel);
		},
		goHome: function() {
			this.oRouter.navTo("Main");
		},
		onAddItem: function() {
			this.oRouter.navTo("View2");
		},
		onNavFilterTable: function() {
			this.oRouter.navTo("View3");
		},
		createColumnConfig: function() {
			var aCols = [];

			aCols.push({
				label: 'ObjectID',
				type: 'string',
				property: '_id',
				scale: 0
			});

			aCols.push({
				label: 'Name',
				property: 'name',
				type: 'string'
			});

			aCols.push({
				label: 'Description',
				property: 'description',
				type: 'string'
			});
			
			aCols.push({
				label: 'Price',
				property: 'price',
				type: 'number'
			});
			
			aCols.push({
				label: 'Vendor',
				property: 'vendor',
				type: 'string'
			});
			
			aCols.push({
				label: 'Company',
				property: 'company',
				type: 'string'
			});
			
			aCols.push({
				label: 'Stock',
				property: 'stock',
				type: 'number'
			});
			
			aCols.push({
				label: 'Available',
				property: 'available',
				type: 'string'
			});
			
			aCols.push({
				label: 'ManufDate',
				property: 'manufDate',
				type: 'date'
			});

			return aCols;
		},
		onExport: function() {
			var aCols, aItems, oSettings;

			aCols = this.createColumnConfig();
			aItems = this.getView().getModel().getProperty("/");

			oSettings = {
				workbook: { columns: aCols },
				dataSource: aItems
			};

			new Spreadsheet(oSettings)
				.build()
				.then( function() {
					MessageToast.show("Spreadsheet export has finished");
				});
		},
		filterTable: function(sObjectId) {
			var a = JSON.parse(sObjectId), //-- Filter table fields from View3
				tblItem = this.getView().byId('tblItems');

			var oBinding = tblItem.getBinding("items");

			oBinding.filter(null);
			oBinding.aFilters = null;
			oBinding.getModel().refresh(true);

			if (oBinding) {
				var oFilters = [],
					oFilter = null;

				if (a._id) {
					oFilter = new sap.ui.model.Filter("_id", sap.ui.model.FilterOperator.StartsWith, a._id);
					oFilters.push(oFilter);
				}

				if (a.name) {
					oFilter = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, a.name);
					oFilters.push(oFilter);
				}

				if (a.description) {
					oFilter = new sap.ui.model.Filter("description", sap.ui.model.FilterOperator.Contains, a.description);
					oFilters.push(oFilter);
				}

				if (a.company) {
					oFilter = new sap.ui.model.Filter("company", sap.ui.model.FilterOperator.Contains, a.company);
					oFilters.push(oFilter);
				}

				if (a.manufDate) {
					var oDate = new Date(a.manufDate),
						formatDate = formatter.factoryDate(oDate);
					oFilter = new sap.ui.model.Filter("manufDate", sap.ui.model.FilterOperator.BT, formatDate + "T00:00:00", formatDate +
						"T23:59:59");

					oFilters.push(oFilter);
				}

				if (oFilters.length > 0) {
					var filterObj = new sap.ui.model.Filter(oFilters, false);
					oBinding.filter(filterObj);
				}
			}
		}
	});
});