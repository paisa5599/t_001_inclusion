sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function(Controller, JSONModel) {
	"use strict";

	return Controller.extend("com.inclusion.T_001_INCLUSION_DEV.view.controller.View3", {
		oRouter: null,
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.inclusion.T_001_INCLUSION_DEV.view.view.View3
		 */
		onInit: function() {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var oDataModel = {
				"_id": null,
				"description": null,
				"manufDate": null
			};

			var oModel = new JSONModel();
			oModel.setData(oDataModel);

			this.getView().setModel(oModel);
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.inclusion.T_001_INCLUSION_DEV.view.view.View3
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.inclusion.T_001_INCLUSION_DEV.view.view.View3
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.inclusion.T_001_INCLUSION_DEV.view.view.View3
		 */
		//	onExit: function() {
		//
		//	}
		onFilterTable: function() {
			var oDataModel = this.getView().getModel().getProperty("/"); // Object Model View.
			
			this.oRouter.navTo("View1Filter", {
				filter: JSON.stringify(oDataModel)
			});
		},
		goBack: function() {
			this.oRouter.navTo("View1");
		}
	});
});