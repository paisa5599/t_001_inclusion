sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"com/inclusion/T_001_INCLUSION_DEV/model/formatter",
	"sap/ui/model/json/JSONModel"
], function(Controller, formatter, JSONModel) {
	"use strict";

	return Controller.extend("com.inclusion.T_001_INCLUSION_DEV.view.controller.View2", {
		oRouter: null,
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.inclusion.T_001_INCLUSION_DEV.view.view.View2
		 */
		onInit: function() {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var oDataModel = {
				"_id": "",
				"name": "",
				"description": "",
				"price": 0,
				"vendor": "",
				"vendorImg": "person-1.svg",
				"company": "",
				"stock": 0,
				"available": true,
				"manufDate": new Date()
			};

			var oModel = new JSONModel();
			oModel.setData(oDataModel);

			this.getView().setModel(oModel);
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.inclusion.T_001_INCLUSION_DEV.view.view.View2
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.inclusion.T_001_INCLUSION_DEV.view.view.View2
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.inclusion.T_001_INCLUSION_DEV.view.view.View2
		 */
		//	onExit: function() {
		//
		//	}
		onGenerateObID: function() {
			var _id = formatter.generateOID();
			this.getView().getModel().setProperty("/_id", _id);
		},
		onCreateItem: function() {
			var oItem = this.getView().getModel().getData();
			oItem.price = oItem.price + " - ARS"; // AFGO: Added --ARS for Argentina Pesos.

			this.oRouter.navTo("View1", {
				row: JSON.stringify(oItem)
			});
		},
		goBack: function() {
			this.oRouter.navTo("View1");
		}
	});
});